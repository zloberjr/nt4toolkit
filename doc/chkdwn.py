execfile("imports.py")
import imports
netaddr_dest_http = "https://pypi.python.org/packages/source/n/netaddr/netaddr-0.7.12.zip"
netaddr_path = "modules/netaddr/"
scapy_dest_http = "http://www.secdev.org/projects/scapy/files/scapy-latest.zip"
scapy_path = "modules/scapy/"
pysmb_dest_http = "https://pypi.python.org/packages/source/p/pysmb/pysmb-1.1.13.zip#md5=9032f8069159672ef0d7c5ae3d22f712"
pysmb_path= "modules/pysmb/"
FNULL = open(os.devnull, 'w')
#modul za pobiranje z HTTP
def downzip(httpdestination, filename, module):

	#print "\n[*] Downloading", filename
	urllib.urlretrieve(httpdestination, filename)
	#print "[+] Successfully downloaded.\n"
	time.sleep(0.1)

	#except:
	#	print "[-] Unable to download file."
	#	sys.exit()

	#print "[*] Extracting", filename
	extractto = "./" + module
	unzip(filename, extractto)
	#print "[+] Successfully extracted.\n"

	unzipped = True
	#except:
	#	print "[-] Extraction failed."
	#	sys.exit()


if imports.foundnetaddr == False:
	if os.path.exists("./modules/"):
		pass		
	else:
		os.mkdir("modules/", 0755);
	
	os.chdir("modules")
	downzip(netaddr_dest_http, "netaddr.zip", "netaddr")
	os.chdir("netaddr")
	netaddrdir = os.listdir(".")
	os.chdir(netaddrdir[0])
	try:
		retcode = subprocess.call(['python', 'setup.py', 'install'], stdout=FNULL, stderr=subprocess.STDOUT)
	except:
		print "[-] Couldn't install netaddr module"
		
	os.chdir("../../../")	

else:
	pass

if imports.foundscapy == False:

	if os.path.exists("./modules/"):
		pass		
	else:
		os.mkdir("modules/", 0755);

	os.chdir("modules")	
	time.sleep(0.1)
	downzip(scapy_dest_http, "scapy.zip", "scapy")
	os.chdir("scapy")
	scapydir = os.listdir(".")
	os.chdir(scapydir[0])
	try:
	
		retcode = subprocess.call(['python', 'setup.py', 'install'], stdout=FNULL, stderr=subprocess.STDOUT)
	except:
		print "[-] Couldn't install scapy module"
	os.chdir("../../../")
	
	
else:
	pass
	

if imports.foundpysmb == False:

	if os.path.exists("./modules/"):
		pass		
	else:
		os.mkdir("modules/", 0755);

	os.chdir("modules")	
	time.sleep(0.1)
	downzip(pysmb_dest_http, "pysmb-1.1.13.zip", "pysmb")
	os.chdir("scapy")
	print "succeed"
	try:
	
		retcode = subprocess.call(['python', 'setup.py', 'install'], stdout=FNULL, stderr=subprocess.STDOUT)
	except:
		print "[-] Couldn't install scapy module"
	os.chdir("../../../")
	
else:
	pass
