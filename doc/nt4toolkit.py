#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import getpass
sys.dont_write_bytecode = True
user = getpass.getuser()
if user == "root":
	pass
else:
	print "[-] Run again as root."
	sys.exit()
#uvažanje modulov

from arpmonitor import arp_monitor_callback
from unzip import unzip
execfile("imports.py")
execfile("chkdwn.py")

#par spremenljivk
uphosts = []


os.system("clear") #pocisti zaslon

#modul za arp monitoring
#def arp_monitor_callback(pkt):
#	if ARP in pkt and pkt[ARP].op in (1,2): #who-has or is-at
#		return pkt.sprintf("%ARP.hwsrc% %ARP.psrc%")

#modul za dodajanje spremenljivk na list
def append1(tsoh):
	uphosts.append(tsoh)

#nekaj spremenljivk
lifeline = re.compile(r"(\d) received")
report = ("No response","Partial Response","Alive")

#glede na spremenljiko foundscapy ali foundnetadddr, preveri ali modul obstaja, če ne ga prenese iz HTTP streznika

#izbrise mapo /modules in počisti zaslon
shutil.rmtree("./modules", ignore_errors=True)

os.system("clear")

#logo vmesnika in menu
print "\n"
print " ███╗   ██╗███████╗████████╗███████╗ ██████╗██╗  ██╗███╗   ██╗"
print " ████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝██║  ██║████╗  ██║"
print " ██╔██╗ ██║█████╗     ██║   ███████╗██║     ███████║██╔██╗ ██║"
print " ██║╚██╗██║██╔══╝     ██║   ╚════██║██║     ╚════██║██║╚██╗██║"
print " ██║ ╚████║███████╗   ██║   ███████║╚██████╗     ██║██║ ╚████║"
print " ╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝ ╚═════╝     ╚═╝╚═╝  ╚═══╝"
time.sleep(0.2)		
print " [1] Network scan"
print " [2] Portscan(single host)"
print " [3] ARP Monitoring"
print " [4] Sniff to file."



cnt = 0

while cnt == 0:
	
	choice = raw_input("\n\n [?] Selection: ") #prebere vnešen tekst in ga shrani v spremenljivko 'choice'
	
	if choice == "1": #če je izbira '1'
		ip_sel = raw_input(" [?] Enter IP subnet(ex. 1.1.1.1/16): ") #prebere vnešen tekst(IP) in ga shrani v spremenljivko 'ip_sel'
		ip_ran = IPNetwork(ip_sel) #konvertira ip_sel v range, modula netaddr
		ip_list = list(ip_ran) #konvertira ip_ran v list
		ips = len(ip_list) #shrani število ip naslovov v 'ips'
		print " [*] Going to scan %s IP Addresses" % (ips) #natisne poved, katera pove koliko ip naslovov ima računalnik namen skenirati.
		for host in IPSet([ip_ran]):#for zanka
			ping = os.popen("ping -q -c2 -w2 "+str(host),"r") #ping ukaz
			while 1: #while zanka
				line = ping.readline() #preberi
				if not line: break #ce line-a ni
				got = re.findall(lifeline,line) #najdi vse
				if "2" in got:#ce je stevilka 2 v got
					append1(host) #dodaj ip na list online host-ov
					#koncaj loop
					cnt = 1
					break
	#izbira 2
	elif choice == "2":
		portscnhost = raw_input("Enter IP: ") #prebere vnešen tekst ter ga shrani v spremenljivko 'portscnhost'
		strt = raw_input("Starting port: ") #zacetni port
		stp = raw_input("Ending port: ") #koncni port
		q = Queue() # spremenljivka
		#threading zanka
		for x in range(30):
    			t = threading.Thread(target=threader)
    			t.daemon = True
    			t.start()

		#zazeni program v več nitih na procesorju
		for worker in range(int(strt),int(stp)):
			
    			q.put(worker)
	
		q.join()
		
	#izbira 3
	elif choice == "3":
		sniff(prn=arp_monitor_callback, filter="arp", store=0)#algoritem za ARP Monitoring
		break
	#vse ostalo
	else:
		print "[-] Invalid selection." #neveljaven izbor
		cnt = 0
