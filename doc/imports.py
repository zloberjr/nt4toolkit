import socket
import os
import sys
import time
import pprint
import subprocess
import imp
import re
import urllib
import zipfile
import getpass
import threading
import shutil
import random
#from download import downzip

try:
	from pysmb import *
	foundpysmb = True
except:
	foundpysmb = False

try:
	from scapy.all import *
	foundscapy = True
except:
	foundscapy = False
try:
	from netaddr import *
	foundnetaddr = True
except:
	foundnetaddr = False
